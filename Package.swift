// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "GamingCloudCore",
    products: [
        .library(
            name: "GamingCloudCore",
            targets: ["GamingCloudCore"])
    ],
    dependencies: [
        .package(url: "git@gitlab.com:mancala/gaming-core.git", from: "1.0.1"),
    ],
    targets: [
        .target(
            name: "GamingCloudCore",
            dependencies: ["GamingCore"])
    ]
)
