import XCTest
@testable import gaming_cloud_coreTests

XCTMain([
    testCase(gaming_cloud_coreTests.allTests),
])
