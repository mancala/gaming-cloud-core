import XCTest
@testable import gaming_cloud_core

class gaming_cloud_coreTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(gaming_cloud_core().text, "Hello, World!")
    }


    static var allTests = [
        ("testExample", testExample),
    ]
}
