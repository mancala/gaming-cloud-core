//
//  GameServerLobbyMessages.swift
//
//  Created by Michael Sanford on 5/22/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import GamingCore
import SwiftOnSockets

public enum LobbyToGameServerMessageType: MessageType {
    case createGamesRequest = 120
}

/*------------------------------------------------------------------------*/

public struct CreateGameInfo: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    public let numberOfPlayers: Int
    public let gameStartupInfo: Data
    
    public init(numberOfPlayers: Int, gameStartupInfo: Data) {
        self.numberOfPlayers = numberOfPlayers
        self.gameStartupInfo = gameStartupInfo
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            numberOfPlayers = try unarchiver.decodeInt()
            gameStartupInfo = try unarchiver.decodeData()
        } catch {
            assert(false, "failed to unarchive CreateGameInfo due to bad format")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(numberOfPlayers)
        archiver.encode(gameStartupInfo)
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        return "{ [CreateGameInfo] numberOfPlayers=\(numberOfPlayers) | gameStartupInfo.count=\(gameStartupInfo.count) }"
    }
}

public struct CreateGamesRequest: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    public let type = LobbyToGameServerMessageType.createGamesRequest
    
    public let requestID: UUID
    public let createGameInfos: [CreateGameInfo]
    
    public init(requestID: UUID = UUID(), createGameInfos: [CreateGameInfo]) {
        self.requestID = requestID
        self.createGameInfos = createGameInfos
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            requestID = try unarchiver.decodeUUID()
            let numberOfGames = try unarchiver.decodeInt()
            
            var createGameInfos: [CreateGameInfo] = []
            for _ in 0 ..< numberOfGames {
                guard let createGameInfo = CreateGameInfo(unarchiver: unarchiver) else {
                    assert(false, "failed to unarchive CreateGameInfo due to internal format error")
                    return nil
                }
                createGameInfos.append(createGameInfo)
            }
            self.createGameInfos = createGameInfos
        } catch {
            assert(false, "failed to unarchive CreateGameInfo due to bad format")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(requestID)
        archiver.encode(createGameInfos.count)
        createGameInfos.forEach {
            $0.encode(with: archiver)
        }
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        return "{ [CreateGamesRequest] requestID=\(requestID) | createGameInfos.count=\(createGameInfos.count) }"
    }
    
}

/*------------------------------------------------------------------------*/

public enum GameServerToLobbyMessageType: MessageType {
    case createGameResponse = 140
    case gameServerCheckin = 141
    case gameServerStatusUpdate = 142
}

/*------------------------------------------------------------------------*/

public struct GameClientInfo: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    public let gameTokens: [PlayerID: GameToken]
    public let gameInfo: Data
    
    public init(gameTokens: [PlayerID: GameToken], gameInfo: Data) {
        self.gameTokens = gameTokens
        self.gameInfo = gameInfo
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            var gameTokens: [PlayerID: GameToken] = [:]
            let count = try unarchiver.decodeInt()
            for _ in 0 ..< count {
                let playerID: PlayerID = try unarchiver.decodeInt()
                guard let gameToken = GameToken(unarchiver: unarchiver) else {
                    assert(false, "failed to unarchive GameClientInfo due to internal invalid format")
                    return nil
                }
                gameTokens[playerID] = gameToken
            }
            self.gameTokens = gameTokens
            self.gameInfo = try unarchiver.decodeData()
        } catch {
            assert(false, "failed to unarchive GameClientInfo due to bad format")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(gameTokens.count)
        gameTokens.forEach {
            archiver.encode($0.key)
            $0.value.encode(with: archiver)
        }
        archiver.encode(gameInfo)
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        return "{ [GameClientInfo] numberOfGameTokens=\(gameTokens.count) | gameInfo.count=\(gameInfo.count) }"
    }
}

/*------------------------------------------------------------------*/

public enum CreateGameResult: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    case success(GameClientInfo)
    case failure(String)
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            let isSuccess: Bool = try unarchiver.decodeBool()
            if isSuccess {
                guard let gameClientInfo = GameClientInfo(unarchiver: unarchiver) else {
                    assert(false, "failed to unarchive CreateGameResult due to internal invalid format")
                    return nil
                }
                self = .success(gameClientInfo)
            } else {
                let errorText = try unarchiver.decodeString()
                self = .failure(errorText)
            }
        } catch {
            assert(false, "failed to unarchive CreateGameResult due to bad format")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        switch self {
        case .success(let gameClientInfo):
            archiver.encode(true)
            gameClientInfo.encode(with: archiver)
        case .failure(let errorText):
            archiver.encode(false)
            archiver.encode(errorText)
        }
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        switch self {
        case .success(let gameClientInfo):
            return "{ [CreateGameResult] success: gameClientInfo=\(gameClientInfo) }"
        case .failure(let errorText):
            return "{ [CreateGameResult] failure: errorText=\(errorText) }"
        }
    }
}

public struct CreateGamesResponse: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    public let type = GameServerToLobbyMessageType.createGameResponse
    
    public let requestID: UUID
    public let createGameResults: [CreateGameResult]
    
    public init(requestID: UUID, createGameResults: [CreateGameResult]) {
        self.requestID = requestID
        self.createGameResults = createGameResults
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            self.requestID = try unarchiver.decodeUUID()
            let count = try unarchiver.decodeInt()
            var createGameResults: [CreateGameResult] = []
            for _ in 0 ..< count {
                guard let createGameResult = CreateGameResult(unarchiver: unarchiver) else {
                    assert(false, "failed to unarchive CreateGamesResponse due to internal invalid format")
                    return nil
                }
                createGameResults.append(createGameResult)
            }
            self.createGameResults = createGameResults
        } catch {
            assert(false, "failed to unarchive CreateGamesResponse due to bad format")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(requestID)
        archiver.encode(createGameResults.count)
        createGameResults.forEach {
            $0.encode(with: archiver)
        }
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        return "{ [CreateGamesResponse] requestID=\(requestID) | createGameResults=\(createGameResults) }"
    }
}

/*------------------------------------------------------------------------*/

public struct GameServerStatusUpdate: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    public static let type = GameServerToLobbyMessageType.gameServerStatusUpdate
    
    public let numberOfGames: Int
    public let numberOfUsers: Int
    
    public init(numberOfGames: Int, numberOfUsers: Int) {
        self.numberOfGames = numberOfGames
        self.numberOfUsers = numberOfUsers
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            numberOfGames = try unarchiver.decodeInt()
            numberOfUsers = try unarchiver.decodeInt()
        } catch {
            assert(false, "failed to unarchive GameServerStatusUpdate due to bad format")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(numberOfGames)
        archiver.encode(numberOfUsers)
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        return "{ [GameServerStatusUpdate] numberOfGames=\(numberOfGames) | numberOfUsers=\(numberOfUsers) }"
    }
}

/*------------------------------------------------------------------------*/

public struct GameServerCheckin: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    public static let type = GameServerToLobbyMessageType.gameServerCheckin
    
    public let connectionAddress: IPAddress
    
    public init(connectionAddress: IPAddress) {
        self.connectionAddress = connectionAddress
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            connectionAddress = try unarchiver.decodeIPAddress()
        } catch {
            assert(false, "failed to unarchive GameServerCheckin due to bad format")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(connectionAddress)
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        return "{ [GameServerCheckin] connectionAddress=\(connectionAddress) }"
    }
}
